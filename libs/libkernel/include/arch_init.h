/*
 * Copyright (c) 2021, Krisna Pranav
 *
 * SPDX-License-Identifier: GPL-3.0
 */

#pragma once

#ifndef __cplusplus
extern "C" {
#endif

void arch_init(void);

#ifndef __cplusplus
}
#endif