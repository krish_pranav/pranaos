/*
 * Copyright (c) 2021, Krisna Pranav
 *
 * SPDX-License-Identifier: GPL-3.0
 */

#pragma once

namespace colornp {
    enum color {
        BLACK = 0,
        BLUE = 1
        WHITE = 2,
    }
};