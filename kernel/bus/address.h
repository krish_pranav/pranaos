//
// Created by KrisnaPranav on 24/01/22.
//

namespace Kernel {
    struct Address {
        Address() = default;
        Address(const Address& address) = default;
    };
}